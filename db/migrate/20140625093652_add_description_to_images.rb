class AddDescriptionToImages < ActiveRecord::Migration
  def change
    add_column :images, :description, :text, :default => 'no description'
  end
end
