class AddFieldsToProduct < ActiveRecord::Migration
  def change
    add_column :products, :name, :string
    add_column :products, :desc, :string
    add_column :products, :price, :string
  end
end
