Rajco::Application.routes.draw do
  # mount Monologue::Engine, at: '/blog'
  resources :categories

get 'products/update_image' => 'products#update_image' , :as => :update_image
get 'products/find_update_image/:id' => 'products#find_update_image' , :as => :find_update_image
get 'products/do_update_image/:id' => 'products#do_update_image' , :as => :do_update_image
post 'products/final_update/:id' => 'products#final_update' , :as => :final_update
  #match 'users/sign_up' => redirect('/home')
  resources :products do
    collection do
      
    end
  end

# get 'products/product_search' => 'products#product_search' , :as => :product_search 
  devise_for :users
  resources :admin do
    collection do
      
    end
  end 

get 'home/cataloges' => 'home#cataloges' , :as => :cataloges
#get 'product/update_image' => 'product#update_image' , :as => :update_image
   resources :home do
    collection do
      get 'product_search'
      get 'search'
      get 'aboutus'
      get 'contact'
      get 'store_finder'
      get 'tearms'
      get 'privacy'
      #get 'check_fun'

    end
    member do
      get 'product'
    end
   end
   root :to => 'home#index'
   get 'home/category_view/:id' => 'home#category_view' , :as => :category_view
   get 'home/category/:id' => 'home#category' , :as => :home_cat
   # get 'home/product_view/:id' => 'home#product_view' , :as => :product 
   #get 'home/product_search' => 'home#product_search' , :as => :product_search
 
end
