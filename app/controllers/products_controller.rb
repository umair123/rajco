class ProductsController < ApplicationController
  
  before_filter :authenticate_user!
  def index
    @products = Product.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new
    @catagory = Category.all
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
  end

  # POST /products
  # POST /products.json
  def create
    
    p = params[:product]
    @product = Product.create(name: p['name'] , price: p['price'] , desc: p['desc'] , category_id: p[:category_id])

    
      if @product.save
        p['photo'].each do |picture|      

        @product.images.create(:photo => picture , product_id: @product.id)
      end
      redirect_to :action => "index" 
    end

          expire_page :controller => "home" , :action => "index"
      expire_page :controller => "home" , :action => "category_view"
      expire_page :controller => "home" , :action => "product" 
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
        
    @product = Product.find(params[:id])
    p = params[:product]
    Product.update(@product.id , name: p['name'] , price: p['price'] , desc: p['desc'] , category_id: p[:category_id])
        
        image = @product.images

        image.each_with_index do |photo , i|
         
         Image.update(photo.id , :photo =>p['photo'][i])
        end
        
       redirect_to :action => "index"
        
   

            expire_page :controller => "home" , :action => "index"
      expire_page :controller => "home" , :action => "category_view"
      expire_page :controller => "home" , :action => "product" 
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end

  def update_image
    @products = Product.all
  end

  def find_update_image
    @product = Product.find(params[:id])
    session[:pid] = params[:id]
    @image = @product.images
    puts "heeelllooooooo #{@image}"
  end

  def do_update_image
    @product = Image.find(params[:id])
  end

  def final_update
    puts "fffffffffffff#{params[:des]}"
    puts "fffffffdddddd #{params[:id]}"
    img = Image.find(params[:id])
    img.update_attributes(:description => params[:des])
    redirect_to :action => "find_update_image", :id => session[:pid]
  end

    
end
