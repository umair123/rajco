class Product < ActiveRecord::Base
	extend FriendlyId
	   friendly_id :name, :use =>:slugged

  def should_generate_new_friendly_id?
    new_record?
  end
  attr_accessible :name, :desc, :price , :category_id
  has_many :images, :dependent => :destroy
  belongs_to :category
end
