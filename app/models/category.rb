class Category < ActiveRecord::Base
	 extend FriendlyId
	 #friendly_id :name
  friendly_id :name, :use =>:slugged

  def should_generate_new_friendly_id?
    new_record?
  end
  #rolify

  attr_accessible :name ,:photo, :category_id
  has_attached_file :photo, :styles => { :medium => "400x400>", :thumb => "100x100>" },
	                  :path => "assets/:id/:styles/:basename.:extension",  
             :storage => :s3,
             :s3_credentials => "#{Rails.root}/config/aws.yml"

   has_many :products
end
